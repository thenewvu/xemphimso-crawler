'use strict';

const util    = require('util');
const rq      = require('request');
const cheerio = require('cheerio');

const SEARCH_SITE = 'http://xemphimso.com/tim-kiem/%s/page-1.html';

/**
 * Extract matching movie sites.
 */
function extractSearchResult($) {
  return $('.movie-box .th.tooltip').map((i, e) => $(e).attr('href')).get();
}

/**
 * Extract movie meta in a movie site.
 */
function extractMovieMeta(url, $) {
  const nameInfoH1       = $('.name-info h1');
  const divCoverImg      = $('div.cover img');
  const dtInfoStts       = $('.dt .info_stt .status');
  const dtp2             = $('.dt p:nth-child(2) span');
  const dtp3             = $('.dt p:nth-child(3) span');
  const dtp4             = $('.dt p:nth-child(4) span');
  const movieDescDiv     = $('#movie_description');
  const actorNameAnchors = $('.actor-name-a');
  // @formatter:off
  return {
    url          : url,
    name         : nameInfoH1.text().trim(),
    thumbnail    : {portrait: divCoverImg.attr('src')},
    catalogs     : dtInfoStts.get().map(e => $(e).text().trim()),
    description  : movieDescDiv.text().trim(),
    director     : dtp2.text().trim(),
    totalEpisodes: dtp3.text().endsWith('tập') ? parseInt(dtp3.text()) : undefined,
    releaseYear  : parseInt(dtp4.text()),
    actors       : actorNameAnchors.get().map(e => $(e).text().trim())
  };
  // @formatter:on
}

/**
 * Search matching movie sites by movie name.
 * @param name
 * @param done
 */
function search(name, done) {
  name      = name.replace(' ', '+');
  const url = util.format(SEARCH_SITE, name);
  rq.get(url, (err, res, body) => {
    if (err || res.statusCode !== 200) {
      const errmsg = `Failed to get search site "${url}" (${err || res.statusCode})`;
      return done(new Error(errmsg));
    }

    const $ = cheerio.load(body);
    done(null, extractSearchResult($));
  });
}

/**
 * Crawl a movie site.
 * @param url
 * @param done
 */
function crawl(url, done) {
  rq.get(url, (err, res, body) => {
    if (err || res.statusCode !== 200) {
      const errmsg = `Failed to crawl site "${url}" (${err || res.statusCode})`;
      return done(new Error(errmsg));
    }

    const $ = cheerio.load(body);
    done(null, extractMovieMeta(url, $));
  });
}

module.exports = {
  search,
  crawl
};
